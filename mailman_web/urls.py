from django.conf.urls import include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from django.contrib import admin

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('hyperkitty.views.index.index'))),
    url(r'^mailman3/', include('postorius.urls')),
    url(r'^archives/', include('hyperkitty.urls')),
    # Social Auth
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    # BrowserID
    url(r'', include('django_browserid.urls')),
    # Django admin
    url(r'^admin/', include(admin.site.urls)),
]


# See the hyperkitty.middleware.SSLRedirect class
SSL_URLS = (
    "hyperkitty.urls",
    "social.apps.django_app.urls",
    "django_browserid.urls",
    )
